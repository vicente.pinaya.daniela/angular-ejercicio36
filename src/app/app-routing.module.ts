import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactoComponent } from './components/contacto/contacto.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { PaginaInvalidaComponent } from './components/pagina-invalida/pagina-invalida.component';
import { ServiciosComponent } from './components/servicios/servicios.component';

const routes: Routes = [
  {path:'inicio', component:InicioComponent},
  {path:'servicios',component:ServiciosComponent},
  {path:'contacto', component:ContactoComponent},
  {path:'404.html',component:PaginaInvalidaComponent},
  {path:'**', pathMatch:'full', redirectTo:'404.html'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
